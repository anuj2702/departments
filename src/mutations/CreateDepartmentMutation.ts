import { MutationCallbacks, setNodeValue } from '@saastack/relay'
import { commitMutation, graphql, Variables } from 'react-relay'
import { Disposable, Environment, RecordProxy, RecordSourceSelectorProxy } from 'relay-runtime'
import {
    CreateDepartmentInput,
    CreateDepartmentMutation,
    CreateDepartmentMutationResponse,
    DepartmentInput,
} from '../__generated__/CreateDepartmentMutation.graphql'

const mutation = graphql`
    mutation CreateDepartmentMutation($input: CreateDepartmentInput) {
        createDepartment(input: $input) {
            clientMutationId
            payload {
                id
            }
        }
    }
`

let tempID = 0

const sharedUpdater = (
    store: RecordSourceSelectorProxy,
    node: RecordProxy,
    department: DepartmentInput,
    filters: Variables
) => {}

const commit = (
    environment: Environment,
    variables: Variables,
    department: DepartmentInput,
    callbacks?: MutationCallbacks<DepartmentInput>
): Disposable => {
    const input: CreateDepartmentInput = {
        parent: variables.parent,
        department,
        clientMutationId: `${tempID++}`,
    }

    return commitMutation<CreateDepartmentMutation>(environment, {
        mutation,
        variables: {
            input,
        },
        optimisticUpdater: (store: RecordSourceSelectorProxy) => {
            const id = `client:newDepartment:${tempID++}`
            const node = store.create(id, 'Department')
            setNodeValue(store, node, department)
            sharedUpdater(store, node, department, variables)
        },
        updater: (store: RecordSourceSelectorProxy) => {
            const payload = store.getRootField('createDepartment')
            const node = payload!.getLinkedRecord('payload')
            sharedUpdater(store, node!, department, variables)
        },
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: (response: CreateDepartmentMutationResponse) => {
            if (callbacks && callbacks.onSuccess) {
                callbacks.onSuccess({ ...department, ...response.createDepartment.payload })
            }
        },
    })
}

export default { commit }
