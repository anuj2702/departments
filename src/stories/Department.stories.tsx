import React from 'react'
import Wrapper from './Wrapper'
import DepartmentPage from '../pages/DepartmentPage'
import '../index'

export default {
    title: 'Departments',
    decorators: [(storyFn: () => JSX.Element) => <Wrapper>{storyFn()}</Wrapper>],
}

export const Default = () => <DepartmentPage />
