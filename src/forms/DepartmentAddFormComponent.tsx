import React from 'react'
import { FormProps } from '@saastack/forms/types'
import { Form } from '@saastack/forms'
import { DepartmentInput } from '../__generated__/CreateDepartmentMutation.graphql'

export interface Props extends FormProps<DepartmentInput> {}

const DepartmentAddFormComponent: React.FC<Props> = (props) => {
    return <Form {...props} />
}

export default DepartmentAddFormComponent
