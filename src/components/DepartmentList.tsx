import { ListContainer } from '@saastack/layouts/containers'
import { ListContainerProps } from '@saastack/layouts/types'
import { Trans } from '@lingui/macro'
import { ListItemAvatar, ListItemText } from '@material-ui/core'
import React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { DepartmentList_departments } from '../__generated__/DepartmentList_departments.graphql'

interface Props extends Omit<ListContainerProps<DepartmentList_departments>, 'items'> {
    departments: DepartmentList_departments
}

const DepartmentList: React.FC<Props> = ({ departments, onItemClick, ...props }) => {
    const [visibleColumns, onVisibleColumnsChange] = React.useState(['id'])
    const listProps = {
        render: (e: DepartmentList_departments[0]) => {
            return [<ListItemText key="id" primary={e.id} />]
        },
    }
    const tableProps = {
        config: [
            {
                key: 'id',
                label: <Trans>Id</Trans>,
            },
        ],
        render: {},
        visibleColumns,
        onVisibleColumnsChange,
    }
    return (
        <ListContainer<DepartmentList_departments>
            items={departments}
            listProps={listProps}
            onItemClick={onItemClick}
            tableProps={tableProps}
            {...props}
        />
    )
}

export default createFragmentContainer(DepartmentList, {
    departments: graphql`
        fragment DepartmentList_departments on Department @relay(plural: true) {
            id
        }
    `,
})
