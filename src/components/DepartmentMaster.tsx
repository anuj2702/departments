import { Trans } from '@lingui/macro'
import loadable from '@loadable/component'
import { AddOutlined } from '@material-ui/icons'
import { ActionItem } from '@saastack/components'
import { Layout, LayoutProps } from '@saastack/layouts'
import { PubSub } from '@saastack/pubsub'
import { Routes, useNavigate, useRouteMatch } from '@saastack/router'
import React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { DepartmentMaster_departments } from '../__generated__/DepartmentMaster_departments.graphql'
import namespace from '../namespace'
import DepartmentList from './DepartmentList'

const DepartmentAdd = loadable(() => import('./DepartmentAdd'))
const DepartmentDelete = loadable(() => import('./DepartmentDelete'))
const DepartmentEmptyState = loadable(() => import('./DepartmentEmptyState'))
const DepartmentUpdate = loadable(() => import('./DepartmentUpdate'))
const DepartmentDetail = loadable(() => import('./DepartmentDetail'))

interface Props {
    parent: string
    departments: DepartmentMaster_departments
    layoutProps?: LayoutProps
}

const DepartmentMaster: React.FC<Props> = ({
    layoutProps,
    departments: {
        departments: { department: departments },
    },
    parent,
}) => {
    const variables = { parent }
    const navigate = useNavigate()
    const match = useRouteMatch<{ id: string }>(':id')
    let selected = ''
    if (match?.params.id) {
        try {
            selected = window.atob(match?.params.id)
        } catch (e) {}
    }

    const header = <Trans>Departments</Trans>
    const actions: ActionItem[] = [
        { icon: AddOutlined, onClick: () => navigate('add'), title: <Trans>Add</Trans> },
    ]

    React.useEffect(() => {
        PubSub.publish(namespace.fetch, departments)
    }, [departments])

    const col1 = !departments.length ? (
        <DepartmentEmptyState onAction={() => navigate('add')} />
    ) : (
        <DepartmentList
            highlighted={selected}
            departments={departments}
            onItemClick={(id: string) => navigate(`${window.btoa(id!)}`)}
        />
    )

    return (
        <Layout
            boxed
            type="OneColumnLayout"
            actions={actions}
            header={header}
            col1={col1}
            {...layoutProps}
        >
            <Routes>{/*    Todo: add routes here*/}</Routes>
        </Layout>
    )
}

export default createFragmentContainer(DepartmentMaster, {
    departments: graphql`
        fragment DepartmentMaster_departments on Query
        @argumentDefinitions(parent: { type: "String" }) {
            departments(parent: $parent) {
                department {
                    id
                    ...DepartmentList_departments
                    ...DepartmentDetail_departments
                    ...DepartmentUpdate_departments
                }
            }
        }
    `,
})
