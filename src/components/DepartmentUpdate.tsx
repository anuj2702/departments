import { useRelayEnvironment } from 'react-relay/hooks'
import { useAlert } from '@saastack/core'
import { FormContainerProps, WizardFormContainerProps } from '@saastack/layouts/types'
import { useNavigate, useParams } from '@saastack/router'
import React from 'react'
import { DepartmentInput } from '../__generated__/UpdateDepartmentMutation.graphql'
import UpdateDepartmentMutation from '../mutations/UpdateDepartmentMutation'
import DepartmentUpdateFormComponent from '../forms/DepartmentUpdateFormComponent'
import { createFragmentContainer } from 'react-relay'
import { graphql } from 'relay-runtime'
import { DepartmentUpdate_departments } from '../__generated__/DepartmentUpdate_departments.graphql'
import { Trans } from '@lingui/macro'
import DepartmentAddValidations from '../utils/DepartmentAddValidations'
import { FormContainer } from '@saastack/layouts/containers'
import { PubSub } from '@saastack/pubsub'
import { Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import namespace from '../namespace'
import DepartmentAddInitialValues from '../utils/DepartmentAddInitialValues'

interface Props extends Omit<FormContainerProps, 'formId'> {
    departments: DepartmentUpdate_departments
}

const formId = 'Department-update'
const DepartmentUpdate: React.FC<Props> = ({ departments, ...props }) => {
    const environment = useRelayEnvironment()
    const showAlert = useAlert()
    const navigate = useNavigate()
    const [open, setOpen] = React.useState(true)
    const [loading, setLoading] = React.useState(false)

    const { id } = useParams()
    const department = departments.find((i) => i.id === window.atob(id!))

    const navigateBack = () => navigate('../')
    const handleClose = () => setOpen(false)
    React.useEffect(() => {
        if (!department) {
            handleClose()
        }
    }, [department])
    if (!department) {
        return null
    }

    const onSuccess = (response: any) => {
        PubSub.publish(namespace.update, response)
        setLoading(false)
        showAlert(<Trans>Updated successfully!</Trans>, { variant: 'info' })
        if (props.onClose) {
            props.onClose()
        }
        handleClose()
    }

    const onError = (e: string) => {
        showAlert(e, {
            variant: 'error',
        })
        setLoading(false)
    }

    const handleSubmit = (values: DepartmentInput) => {
        setLoading(true)
        const departmentInput: DepartmentInput = {
            ...values,
        }
        UpdateDepartmentMutation.commit(environment, departmentInput, [], {
            onSuccess,
            onError,
        })
    }

    const initialValues = {
        ...DepartmentAddInitialValues,
        ...department,
    }

    return (
        <FormContainer
            formId={formId}
            header={<Trans>Update Department</Trans>}
            loading={loading}
            onClose={handleClose}
            open={open}
            onExited={navigateBack}
        >
            <DepartmentUpdateFormComponent
                initialValues={initialValues}
                validationSchema={DepartmentAddValidations}
                id={formId}
                onSubmit={handleSubmit}
            />
        </FormContainer>
    )
}

export default createFragmentContainer(DepartmentUpdate, {
    departments: graphql`
        fragment DepartmentUpdate_departments on Department @relay(plural: true) {
            id
        }
    `,
})
