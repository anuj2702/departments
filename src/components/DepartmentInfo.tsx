import React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { DepartmentInfo_department } from '../__generated__/DepartmentInfo_department.graphql'

interface Props {
    department: DepartmentInfo_department
}

const DepartmentInfo: React.FC<Props> = ({ ...props }) => {
    return <></>
}

export default createFragmentContainer(DepartmentInfo, {
    department: graphql`
        fragment DepartmentInfo_department on Department {
            id
        }
    `,
})
