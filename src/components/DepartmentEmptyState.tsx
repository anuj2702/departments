import { EmptyStateContainer } from '@saastack/layouts/containers'
import { EmptyStateContainerProps } from '@saastack/layouts/types'
import React from 'react'
import { Trans } from '@lingui/macro'

interface Props extends EmptyStateContainerProps {}

const DepartmentEmptyState: React.FC<Props> = ({ onAction, ...props }) => {
    const emptyStateProps: EmptyStateContainerProps = {
        image: `${process.env.REACT_APP_SAASTACK_CDN_URL}/images/empty-states/booking-notification-empty-state.png`,
        message: <Trans>Add your first Department</Trans>,
        actionLabel: <Trans>Add Department</Trans>,
        onAction,
    }

    return <EmptyStateContainer {...emptyStateProps} {...props} />
}

export default DepartmentEmptyState
